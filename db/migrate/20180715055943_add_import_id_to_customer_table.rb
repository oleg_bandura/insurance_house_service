class AddImportIdToCustomerTable < ActiveRecord::Migration[5.1]
  def change
    add_reference :customers, :import, foreign_key: true
  end
end
