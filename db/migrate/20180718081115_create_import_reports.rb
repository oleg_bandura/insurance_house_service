class CreateImportReports < ActiveRecord::Migration[5.1]
  def change
    create_table :import_reports do |t|
      t.integer :import_id
      t.text :report
      t.timestamps
    end
  end
end
