class AddCustomerIdToQuote < ActiveRecord::Migration[5.1]
  def change
    add_column(:quotes, :customer_id, :integer)
    add_index(:quotes, :customer_id)
  end
end
