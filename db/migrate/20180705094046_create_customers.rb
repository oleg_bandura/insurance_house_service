# migration create customer
class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :first_name
      t.string :last_name
      t.integer :fico_score
      t.integer :marital_status
      t.date :date_of_birth
      t.string :phone
      t.string :email
      t.timestamps
    end
  end
end
