class CreateImports < ActiveRecord::Migration[5.1] # :nodoc:
  def change
    create_table :imports do |t|
      t.string :file
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
