class CreateFailReasons < ActiveRecord::Migration[5.1]
  def change
    create_table :fail_reasons do |t|
      t.references :import_report, index: true
      t.text :reason
      t.timestamps
    end
  end
end
