class RemoveReport < ActiveRecord::Migration[5.1]
  def change
    remove_column(:import_reports, :report, :text)
  end
end
