class AddTitleToImports < ActiveRecord::Migration[5.1] # :nodoc:
  def change
    add_column :imports, :title, :string
  end
end
