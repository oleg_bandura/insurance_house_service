# migration policies
class CreatePolicies < ActiveRecord::Migration[5.1]
  def change
    create_table :policies do |t|
      t.references :customer, index: true
      t.string :carrier_name
      t.numeric :premium
      t.json :coverages
      t.timestamps
    end
  end
end
