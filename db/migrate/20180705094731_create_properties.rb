# migration Properties
class CreateProperties < ActiveRecord::Migration[5.1]
  def change
    create_table :properties do |t|
      t.references :customer, index: true
      t.integer :property_type
      t.integer :residency_type
      t.string :address_line1
      t.string :city
      t.string :state
      t.string :zip
      t.timestamps
    end
  end
end
