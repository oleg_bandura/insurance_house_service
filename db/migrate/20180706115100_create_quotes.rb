class CreateQuotes < ActiveRecord::Migration[5.1]
  def change
    create_table :quotes do |t|
      t.string :carrier_key
      t.string :status
      t.string :message
      t.integer :premium
      t.json :coverages
      t.references :import, foreign_key: true

      t.timestamps
    end
  end
end

