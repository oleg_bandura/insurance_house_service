# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rails db:seed command (or created
# alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' },
# { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# manager
User.create(role: "manager", email: "manager@gmail.com", first_name: "Alex", last_name: "Bench", password: "1234567")

# agent
p = Partner.create(name: 'Privatbank', key: '123')
User.create(role: "agent", email: "agent@gmail.com", first_name: "Michael", last_name: "Bench", password: "1234567", partner_id: p.id)

import = Import.last
customer = Customer.create!(import: import,
                            first_name: 'Adam',
                            last_name: 'Smith',
                            fico_score: 99,
                            marital_status: 'single',
                            date_of_birth: '1990-01-01',
                            phone: '123-456-7890',
                            email: 'adam@gmail.com')

Property.create!(customer_id: customer.id,
                 property_type: 'single_family_detached',
                 residency_type: 'primary_residence',
                 address_line1: '123 Main Street',
                 city: 'Vacaville',
                 state: 'CA',
                 zip: '95688')

Policy.create!(customer_id: customer.id,
               carrier_name: 'California Insurance Company',
               premium: 999.0,
               coverages: JSON.parse("{\"dwelling_coverage\":123456,\"deductible\":123}"))
