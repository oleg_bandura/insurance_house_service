# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180725155952) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "customers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.integer "fico_score"
    t.integer "marital_status"
    t.date "date_of_birth"
    t.string "phone"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "import_id"
    t.index ["import_id"], name: "index_customers_on_import_id"
  end

  create_table "fail_reasons", force: :cascade do |t|
    t.bigint "import_report_id"
    t.text "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["import_report_id"], name: "index_fail_reasons_on_import_report_id"
  end

  create_table "import_reports", force: :cascade do |t|
    t.integer "import_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "imports", force: :cascade do |t|
    t.string "file"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.bigint "partner_id"
    t.index ["partner_id"], name: "index_imports_on_partner_id"
  end

  create_table "partners", force: :cascade do |t|
    t.string "name"
    t.string "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "policies", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "carrier_name"
    t.decimal "premium"
    t.json "coverages"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_policies_on_customer_id"
  end

  create_table "properties", force: :cascade do |t|
    t.bigint "customer_id"
    t.integer "property_type"
    t.integer "residency_type"
    t.string "address_line1"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_properties_on_customer_id"
  end

  create_table "quotes", force: :cascade do |t|
    t.string "carrier_key"
    t.integer "status"
    t.string "message"
    t.integer "premium"
    t.json "coverages"
    t.bigint "import_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "customer_id"
    t.index ["customer_id"], name: "index_quotes_on_customer_id"
    t.index ["import_id"], name: "index_quotes_on_import_id"
  end

  create_table "users", force: :cascade do |t|
    t.integer "role"
    t.string "first_name"
    t.string "last_name"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.bigint "partner_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["partner_id"], name: "index_users_on_partner_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "customers", "imports"
  add_foreign_key "imports", "partners"
  add_foreign_key "quotes", "imports"
  add_foreign_key "users", "partners"
end
