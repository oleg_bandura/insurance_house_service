require 'rails_helper'

describe ImportsController, type: :controller do
  describe 'GET #new' do
    before do
      get :new
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #create' do
    login_agent
    let(:post_params) { { params: { import: { file: 'spec/fixtures/Bank1.csv' } } } }
    let(:response) { post :create, post_params }
    let(:success) { true }
    let(:failure) { false }
    let(:message) { 'message' }
    let(:import) { create :import }
    let(:result_hash) { { success?: success, failure?: failure, message: message, import: import } }
    let(:result) { OpenStruct.new(result_hash) }
    let(:notice) { 'Import successfully created' }

    before do
      allow(CreateImport).to receive(:call).and_return(result)
    end

    context 'when success' do
      it 'performs redirect' do
        expect(response).to have_http_status(302)
      end
      it { expect(response).to redirect_to result.import }
      it 'shows notice' do
        response
        expect(flash[:notice]).not_to match(notice)
      end
    end

    context 'when failure' do
      let(:success) { false }
      let(:failure) { true }
      it { expect(response).to render_template(:new) }
      it 'shows alert' do
        response
        expect(flash[:alert]).to match(import.errors.full_messages.join)
      end
    end
  end

  describe 'GET #index' do
    context 'when agent' do
      login_agent

      before do
        get :index, params: { user_id: subject.current_user.id }
      end

      it 'should have a current_user' do
        expect(subject.current_user).to_not eq(nil)
      end

      context 'when user has role agent' do
        let(:notice) { "You don't have permissions!" }

        it 'redirects user to root path' do
          expect(response).to redirect_to root_path
        end

        it 'returns notice with error' do
          expect(flash[:notice]).to match(notice)
        end
      end
    end

    context 'when manager' do
      login_manager

      before do
        get :index, params: { user_id: subject.current_user.id }
      end

      it 'get index' do
        expect(response).to render_template('index')
      end
    end
  end

  describe 'GET #parse csv file' do
    login_manager
    let!(:import) { create(:import) }
    let(:parse_csv_file) do
      get :parse_csv_file, params: {
        user_id: subject.current_user.id,
        id: import.id
      } end
    before do
      allow(ParseCsv).to receive(:call)
    end

    it 'get parse csv file' do
      expect(response).to have_http_status(200)
    end

    it 'have arrtibutes' do
      parse_csv_file
      expect(ParseCsv).to have_received(:call).with(import_id: import.id.to_s)
    end

    it 'redirect to imports' do
      expect(parse_csv_file).to redirect_to(imports_path)
    end
  end

  describe '.start_quoting' do
    login_manager
    let(:import) { create(:import) }
    let(:start_quoting) do
      get :start_quoting, params: {
        user_id: subject.current_user.id,
        id: import.id
      }
    end

    before do
      allow(StartQuoting).to receive(:call)
    end

    it 'redirects user to imports path' do
      expect(start_quoting).to redirect_to imports_path
    end

    it 'calls Interactor' do
      start_quoting
      expect(StartQuoting).to have_received(:call).with(import_id: import.id.to_s)
    end

    it 'redirects to imports' do
      start_quoting
      expect(response).to redirect_to(imports_path)
    end
  end

  describe 'GET #generate_report' do
    login_manager

    let(:import) { create(:import) }
    let(:result) { OpenStruct.new(csv: '') }

    before do
      allow(CreateReport).to receive(:call).and_return(result)
      get :generate_report, format: :csv, params: { id: import.id }
    end

    it 'returns a CSV file' do
      expect(response.headers['Content-Type']).to include 'text/csv'
    end

    it 'return content' do
      expect(CreateReport).to have_received(:call).with(import_id: import.id.to_s)
    end
  end
end
