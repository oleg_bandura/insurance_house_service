require 'rails_helper'

RSpec.describe QuotesController, type: :controller do
  describe 'Quotes controller request specs' do
    context 'GET #index' do
      let(:import) { create :import }
      let(:quote) { create(:quote, import: import) }
      it 'assigns @quote' do
        get :index, params: { id: quote, import_id: import.id }
        expect(assigns(:quotes)).to eq([quote])
      end

      it 'renders the index template' do
        get :index, params: { id: quote, import_id: import.id }
        expect(response.status).to eq(200)
      end
    end

    context 'GET #show' do
      let(:import) { create :import }
      let(:quote) { create :quote }
      it 'assigns @quote' do
        get :show, params: { id: quote, import_id: import.id }
        expect(assigns(:quote)).to eq(quote)
      end

      it 'render the show template' do
        get :show, params: { id: quote, import_id: import.id }
        expect(response.status).to eq(200)
      end
    end
  end
end
