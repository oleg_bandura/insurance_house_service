module ControllerMacros
  def login_manager
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      manager = FactoryBot.create(:manager)
      sign_in(manager, scope: :user)
    end
  end

  def login_agent
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = FactoryBot.create(:user)
      sign_in user
    end
  end
end
