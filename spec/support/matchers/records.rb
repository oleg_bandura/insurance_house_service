def create_model(model, options = {})
  change { model.where(options).count }
end

def change_model(model, method)
  change { model.reload.send(method) }
end
