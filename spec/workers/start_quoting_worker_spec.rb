require 'rails_helper'
describe StartQuotingWorker, type: :fixture do
  describe '.perform' do
    let(:import) { create(:import) }
    let(:customer) { create(:customer, import: import) }
    let(:import_id) { import.id }

    it 'job should be processed in' do
      is_expected.to be_processed_in :default
    end

    it 'enqueues 2 retry workers' do
      expect(described_class).to be_retryable(2)
    end

    it 'set arguments in worker' do
      described_class.perform_async(import_id)
      expect(described_class).to have_enqueued_sidekiq_job(import_id)
    end

    it 'set nil arguments in worker' do
      described_class.perform_async(nil)
      expect(described_class).to_not have_enqueued_sidekiq_job(import_id)
    end
  end
end
