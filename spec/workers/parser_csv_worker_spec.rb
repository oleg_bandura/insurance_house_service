require 'rails_helper'
describe ParseCsvWorker, type: :fixture do
  describe '.perform' do
    let(:row) { 'Eddie,Heller' }
    let(:import) { create(:import) }
    let(:import_id) { import.id }
    let(:action) { CreateCustomer.call(data: row, import_id: import.id) }
    it 'job should be processed in' do
      is_expected.to be_processed_in :default
    end

    it 'enqueues 2 retry workers' do
      expect(ParseCsvWorker).to be_retryable(2)
    end

    it 'set arguments in worker' do
      ParseCsvWorker.perform_async(row, import_id)
      expect(ParseCsvWorker).to have_enqueued_sidekiq_job(row, import_id)
    end

    it 'set nil arguments in worker' do
      ParseCsvWorker.perform_async(nil, nil)
      expect(ParseCsvWorker).to_not have_enqueued_sidekiq_job(row, import_id)
    end
  end
end
