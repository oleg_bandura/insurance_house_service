require 'rails_helper'

describe StartQuoting, type: :feature do
  describe '.call' do
    subject(:start_quoting) { described_class.call(import_id: import_id) }
    let(:import) { create(:import) }
    let(:import_id) { import.id }
    let!(:customer) { create(:customer, import_id: import.id) }
    let!(:customer2) { create(:customer, import_id: import.id) }

    before do
      allow(StartQuotingWorker).to receive(:perform_async)
    end

    it { is_expected.to be_a_success }

    it 'calls worker' do
      start_quoting
      expect(StartQuotingWorker).to have_received(:perform_async).twice
    end
  end
end
