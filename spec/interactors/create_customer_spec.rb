require 'rails_helper'

describe CreateCustomer do
  subject(:create_customer) { described_class.new }
  let(:import) { create(:import) }
  let(:action) { described_class.call(data: { customer: 'data' }, import_id: import.id) }

  let(:partner) { create(:partner) }
  let(:partner_key) { partner.key }
  let(:first_name) { 'Walker' }
  let(:address1) { '28236 Wilmer Place' }
  let(:premium) { '500' }

  let(:data) do
    {
      customer: {
        first_name: first_name,
        last_name: 'Bernhard',
        fico_score: '387',
        marital_status: 'married',
        date_of_birth: '1988-07-05',
        phone: '296-901-1043',
        email: 'catherine_stehr@ward.com'
      },
      property: {
        property_type: 'manufactured_home',
        residency_type: 'investment',
        address: {
          address1: address1,
          city: 'Lake Josephina',
          state: 'ME',
          zip: '04734-0058'
        }
      },
      policy: {
        carrier_name: 'Teresia Jones',
        premium: premium,
        coverages: {
          'dwelling_coverage' => 131_011,
          'deductible' => 1654
        }
      }
    }
  end

  before do
    allow(Mappers::MapperFinder).to receive(:find_by_partner_key).and_return(Mappers::Privatbank)
    allow(Mappers::Privatbank).to receive(:format).and_return(data)
  end

  it 'creates customer' do
    expect { action }.to change { Customer.count }.by(1)
  end
  it 'creates property' do
    expect { action }.to change { Property.count }.by(1)
  end
  it 'creates policy' do
    expect { action }.to change { Policy.count }.by(1)
  end

  it 'does not create fail reason record' do
    expect { action }.not_to create_model(FailReason)
  end

  context 'with invalid data' do
    let(:first_name) { nil }
    let(:address1) { nil }
    let(:premium) { nil }
    let!(:create_report) { create(:import_report, import: import) }

    it 'does not save customer' do
      expect { action }.not_to create_model(Customer)
    end

    it 'creates fail reason record' do
      error = "Property address line1 can't be blank, Policy premium can't be "\
      "blank, First name can't be blank"

      expect { action }.to create_model(FailReason, reason: error)
    end
  end
end
