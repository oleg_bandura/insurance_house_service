require 'rails_helper'

describe CreateImport do
  subject(:interactor) { described_class.call(params: params) }

  let(:params) { { file: file, title: 'New file', partner_id: user.partner_id } }
  let(:user) { create(:user) }
  let(:file) { File.open('spec/fixtures/Bank1.csv') }

  describe '.call' do
    context 'negative case' do
      let(:file) { nil }
      let(:error_message) { "File can't be blank" }
      it { expect(interactor.failure?).to be true }
      it { expect(interactor.message).to match(error_message) }
    end

    context 'positive case' do
      it { expect(interactor.success?).to be true }
      it 'creates import' do
        expect { interactor }.to change(Import, :count).by(1)
      end
    end
  end
end
