require 'rails_helper'

describe CreateQuote do
  subject(:create_quote) { described_class.call(customer: customer, import_id: import.id) }

  let(:customer) { create(:customer) }
  let(:import) { create(:import) }
  let(:build) { instance_double(Quoting::Builder, build: { 'data' => 1 }) }
  let(:requester) { instance_double(Quoting::Requester, request: response) }

  before do
    allow(Quoting::Builder).to receive(:new).and_return(build)
    allow(Quoting::Requester).to receive(:new).and_return(requester)
  end

  let(:response) do
    { 'status' => 'success',
      'carrier_key' => 'safeco',
      'message' => 'Ok',
      'quote' =>
         { 'premium' => 911,
           'deductible' => 1623,
           'dwelling_coverage' => 9896 } }
  end

  let(:quote_data) do
    {
      carrier_key: 'safeco',
      status: 'success',
      message: 'Ok',
      premium: 911,
      import_id: import.id
    }
  end

  it 'creates quote' do
    expect { create_quote }.to change { Quote.where(quote_data).count }.by(1)
  end

  it 'creates quote with correct coverages' do
    create_quote
    quote = Quote.last
    expect(quote.coverages).to eq('deductible' => 1623, 'dwelling_coverage' => 9896)
  end
end
