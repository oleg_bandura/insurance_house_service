require 'rails_helper'

describe ParseCsv, type: :feature do
  describe '.call' do
    subject(:parse_csv) { described_class.call(import_id: import_id) }

    before do
      allow(ParseCsvWorker).to receive(:perform_async)
    end

    let(:import) { create(:import) }
    let(:import_id) { import.id }

    it { is_expected.to be_a_success }

    it 'creates import report' do
      expect { parse_csv }.to change { ImportReport.count }.by(1)
    end

    it 'updates import status' do
      parse_csv
      expect(import.status).to eq('started')
    end

    it 'calls worker' do
      parse_csv
      expect(ParseCsvWorker).to have_received(:perform_async).twice
    end
  end
end
