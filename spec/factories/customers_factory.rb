FactoryBot.define do
  factory :customer do
    import
    first_name 'Eddie'
    last_name 'Heller'
    fico_score 327
    marital_status 'married'
    date_of_birth '1988-07-05'
    phone '556-629-9597'
    email 'marty@hauck.biz'
  end
end
