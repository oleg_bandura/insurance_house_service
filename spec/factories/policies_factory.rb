FactoryBot.define do
  factory :policy do
    customer
    carrier_name 'Jody Cummerata'
    premium 551.0
    coverages "{'dwelling_coverage'=>158_192, 'deductible'=>176_3}"
  end
end
