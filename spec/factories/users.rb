FactoryBot.define do
  factory :user do
    partner
    role 'agent'
    first_name 'John'
    last_name 'Doe'
    password 'password'
    email { FFaker::Internet.email }

    trait :manager do
      role 'manager'
    end

    factory :manager, traits: %i[manager]
  end
end
