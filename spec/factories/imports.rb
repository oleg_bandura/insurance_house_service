FactoryBot.define do
  factory :import do
    partner
    file Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'fixtures', 'Bank1.csv'))
    status 1
    title 'Test file'
  end
end
