FactoryBot.define do
  factory :property do
    customer
    property_type 'condominium'
    residency_type 'second_home'
    address_line1 '13283 Smitham Hills'
    city 'Marvinchester'
    state 'OR'
    zip '80549'
  end
end
