FactoryBot.define do
  factory :quote do
    carrier_key { FFaker::Company.name }
    status 'success'
    message { FFaker::Boolean.maybe }
    premium { (200..1000).to_a.sample }
    coverages(dwelling_coverage: 500_000, deductible: 5000)
    customer
    import
  end
end
