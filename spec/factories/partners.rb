FactoryBot.define do
  factory :partner do
    name 'PrivatBank'
    key { SecureRandom.hex(7) }
  end
end
