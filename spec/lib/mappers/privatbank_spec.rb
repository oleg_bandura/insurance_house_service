require 'rails_helper'

RSpec.describe Mappers::Privatbank do
  let(:row) do
    { 'first_name' => 'Eddie',
      'last_name' => 'Heller',
      'fico_score' => '327',
      'marital_status' => 'married',
      'date_of_birth' => '1988-07-05',
      'phone' => '556-629-9597',
      'email' => 'marty@hauck.biz',
      'property_type' => 'condominium',
      'residency_type' => 'second_home',
      'address_line1' => '13283 Smitham Hills',
      'city' => 'Marvinchester',
      'state' => 'OR',
      'zip' => '80549',
      'carrier_name' => 'Jody Cummerata',
      'premium' => '551.0',
      'coverages' => '{"dwelling_coverage"=>158192, "deductible"=>1763}' }
  end

  let(:result) do
    { customer: {
      first_name: row['first_name'],
      last_name: row['last_name'],
      fico_score: row['fico_score'],
      marital_status: row['marital_status'],
      date_of_birth: row['date_of_birth'],
      phone: row['phone'],
      email: row['email']
    },
      property: {
        property_type: row['property_type'],
        residency_type: row['residency_type'],
        address: {
          address1: row['address_line1'],
          city: row['city'],
          state: row['state'],
          zip: row['zip']
        }
      },
      policy: {
        carrier_name: row['carrier_name'],
        premium: row['premium'],
        coverages: { 'dwelling_coverage' => 158_192, 'deductible' => 1763 }
      } }
  end

  describe '#format' do
    it 'returns formatted hash' do
      expect(described_class.format(row)).to eq(result)
    end
  end
end
