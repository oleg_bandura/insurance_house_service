# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mappers::MapperFinder do
  let!(:partner) { create(:partner) }
  let(:key) { partner.key }
  let(:mapper_class) { Mappers::Privatbank }

  describe '.find_by_partner_key' do
    it 'returns correct mapper' do
      expect(described_class.find_by_partner_key(key)).to eq(mapper_class)
    end
  end
end
