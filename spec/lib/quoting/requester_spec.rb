require 'rails_helper'

describe ::Quoting::Requester do
  let(:requester) { described_class.new(data) }
  let(:request) { requester.request }
  let(:first_name) { 'Imanuel' }

  let(:data) do
    {
      'customer' => {
        'first_name' => first_name,
        'last_name' => 'Bernhard',
        'fico_score' => '387',
        'marital_status' => 'married',
        'date_of_birth' => '07/05/1988',
        'phone' => '296-901-1043',
        'email' => 'catherine_stehr@ward.com'
      }, 'property' => {
        'address' => {
          'address1' => '28236 Wilmer Place',
          'city' => 'Lake Josephina',
          'state' => 'ME',
          'zip' => '04734-0058'
        }, 'property_type' => 'manufactured_home',
        'residency_type' => 'investment'
      },
      'policy' => {
        'carrier_name' => 'Teresia Jones',
        'premium' => '553.0',
        'coverages' => { 'dwelling_coverage' => 131_011, 'deductible' => 1654 }
      }
    }
  end

  it 'Returns success' do
    VCR.use_cassette('success_test_request') do
      expect(request['status']).to eq('success')
    end
  end

  context 'with invalid data' do
    let(:first_name) { nil }

    it 'returns error' do
      VCR.use_cassette('missing_test_request') do
        response = JSON.parse(request.body)
        expect(response[0]['params'][0]).to eq('customer[first_name]')
        expect(response[0]['messages']).to eq(['is empty'])
      end
    end
  end
end
