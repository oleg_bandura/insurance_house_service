# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Quoting::Builder do
  let!(:customer) { create(:customer) }
  let!(:property) { create(:property, customer: customer) }
  let!(:policy) { create(:policy, customer: customer) }
  let(:result) do
    { customer: {
      first_name: customer.first_name,
      last_name: customer.last_name,
      fico_score: customer.fico_score,
      date_of_birth: customer.date_of_birth.strftime('%m/%d/%Y'),
      marital_status: customer.marital_status,
      phone: customer.phone,
      email: customer.email
    },
      property: {
        address: {
          address1: property.address_line1,
          city: property.city,
          state: property.state,
          zip: property.zip
        },
        property_type: property.property_type,
        residency_type: property.residency_type
      },
      policy: {
        carrier_name: policy.carrier_name,
        premium: policy.premium,
        coverages: policy.coverages
      } }
  end

  describe '#build' do
    it 'returns expected hash' do
      expect(described_class.new(customer).build).to eq(result)
    end
  end
end
