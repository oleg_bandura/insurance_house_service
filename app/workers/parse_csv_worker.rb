class ParseCsvWorker
  include Sidekiq::Worker
  sidekiq_options retry: 2
  def perform(row, import_id)
    CreateCustomer.call(data: row, import_id: import_id)
  end
end
