class StartQuotingWorker
  include Sidekiq::Worker
  sidekiq_options retry: 2
  def perform(customer_id, import_id)
    customer = Customer.find(customer_id)
    CreateQuote.call(customer: customer, import_id: import_id)
  end
end
