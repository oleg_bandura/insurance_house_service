# class application controller
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  private

  def after_sign_in_path_for(resource)
    if resource.manager?
      imports_path
    elsif resource.agent?
      new_import_path
    else
      pages_error_path
    end
  end
end
