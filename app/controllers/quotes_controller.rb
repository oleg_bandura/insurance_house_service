class QuotesController < ApplicationController
  def index
    @quotes = set_import.quotes.page params[:page]
  end

  def show
    @quote = set_quote
  end

  private

  def set_quote
    @quote = Quote.find(params[:id])
  end

  def set_import
    @import = Import.find(params[:import_id])
  end
end
