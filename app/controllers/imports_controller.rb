class ImportsController < ApplicationController # :nodoc:
  before_action :manager?, except: %i[new show create]
  def index
    @imports = Import.all.page params[:page]
  end

  def new
    @import = Import.new
  end

  def create
    result = CreateImport.call(params: import_data)
    if result.success?
      redirect_to result.import
    else
      flash[:alert] = result.message
      render :new
    end
  end

  def show; end

  def parse_csv_file
    ParseCsv.call(import_id: params[:id])
    redirect_to imports_path
  end

  def start_quoting
    StartQuoting.call(import_id: params[:id])
    redirect_to imports_path
  end

  def generate_report
    report = CreateReport.call(import_id: params[:id])
    send_data report.csv, filename: 'Report.csv'
  end

  private

  def manager?
    if current_user.manager?
      true
    else
      redirect_to root_path, notice: "You don't have permissions!"
    end
  end

  def import_params
    params.require(:import).permit(:title, :file)
  end

  def import_data
    import_params.merge(partner_id: current_user.partner_id)
  end
end
