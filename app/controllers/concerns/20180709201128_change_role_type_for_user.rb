# ChangeRoleTypeForUsers
class ChangeRoleTypeForUser < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :role, :integer, using: 'role::integer'
  end
end
