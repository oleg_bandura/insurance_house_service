class Customer < ApplicationRecord
  has_one :property, autosave: true
  has_one :policy, autosave: true
  has_one :quote
  belongs_to :import

  enum marital_status: %I[
    married
    separated
    single
  ]

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :fico_score, presence: true
  validates :marital_status, presence: true
  validates :date_of_birth, presence: true
  validates :phone, presence: true
  validates :email, presence: true

  scope :with_success_quotes, lambda {
    left_outer_joins(:quote).where(
      quotes: { status: 'success' }
    )
  }

  delegate :premium, to: :policy, prefix: 'previous'
  delegate :premium, to: :quote, prefix: true
end
