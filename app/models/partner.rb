class Partner < ApplicationRecord
  has_many :imports
  has_many :users

  validates_uniqueness_of :key
end
