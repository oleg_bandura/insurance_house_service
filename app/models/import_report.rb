class ImportReport < ApplicationRecord
  belongs_to :import
  has_many :fail_reasons
end
