class Quote < ApplicationRecord
  belongs_to :import
  belongs_to :customer
  paginates_per 20

  enum status: %I[success fail]
end
