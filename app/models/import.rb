class Import < ApplicationRecord # :nodoc:
  belongs_to :partner

  has_many :quotes
  has_many :customers
  has_one :import_report
  has_many :fail_reasons, through: :import_report

  validates :file,   presence: true
  validates :status, presence: true
  validates :title,  presence: true

  enum status: %i[created started finished]

  mount_uploader :file, ImportUploader

  def file_path
    file.file.url
  end
end
