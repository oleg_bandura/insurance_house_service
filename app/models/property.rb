class Property < ApplicationRecord
  belongs_to :customer

  enum property_type: %I[
    condominium
    multifamily
    townhouse
    mobile_home
    duplex
    manufactured_home
    triplex
    rowhome
    apartment
    farm
    land_and_developed_lots
    single_family_detached
    pud
    four_plex
    other
  ]
  enum residency_type: %I[
    second_home
    primary_residence
    investment
  ]

  validates :property_type, presence: true
  validates :residency_type, presence: true
  validates :address_line1, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :zip, presence: true

  def full_address
    "#{address_line1}, #{city}, #{state} #{zip}"
  end
end
