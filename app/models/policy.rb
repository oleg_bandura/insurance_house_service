# model Policy
class Policy < ApplicationRecord
  belongs_to :customer

  validates :carrier_name, presence: true
  validates :premium, presence: true
  validates :coverages, presence: true
end
