class CreateReport
  include Interactor

  delegate :import_id, to: :context

  def call
    context.fail!(message: 'Report cannot be generated without customers.') if customers.blank?
    context.csv = generate_csv
  end

  private

  def generate_csv
    CSV.generate do |csv|
      csv << headers
      customers.each do |customer|
        csv << [
          customer.first_name, customer.last_name,
          customer.property.full_address,
          customer.previous_premium, customer.quote_premium
        ]
      end
    end
  end

  def import
    Import.find(import_id)
  end

  def customers
    @customers ||= import.customers.with_success_quotes
  end

  def headers
    %w[first_name last_name full_address previous_premium new_premium]
  end
end
