class CreateImport # :nodoc:
  include Interactor

  def call
    import = Import.new(context.params)
    if import.save
      context.import = import
    else
      context.fail!(message: import.errors.full_messages.join)
    end
  end
end
