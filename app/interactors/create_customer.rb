class CreateCustomer
  include Interactor
  delegate :data, :import_id, to: :context

  def call
    formatted_data = format_data
    customer = create_customer(formatted_data)
    create_property(customer, formatted_data)
    create_policy(customer, formatted_data)
    logger(customer) unless customer.save
  end

  private

  def format_data
    partner_key = import.partner.key
    mapper_class = Mappers::MapperFinder.find_by_partner_key(partner_key)
    mapper_class.format(data)
  end

  def import
    @import ||= Import.find(import_id)
  end

  def customer_info(data)
    {
      first_name: data[:customer][:first_name],
      last_name: data[:customer][:last_name],
      fico_score: data[:customer][:fico_score],
      marital_status: data[:customer][:marital_status],
      date_of_birth: data[:customer][:date_of_birth],
      phone: data[:customer][:phone],
      email: data[:customer][:email]
    }
  end

  def create_customer(data)
    import.customers.build(customer_info(data))
  end

  def property_info(data)
    {
      property_type: data[:property][:property_type],
      residency_type: data[:property][:residency_type],
      address_line1: data[:property][:address][:address1],
      city: data[:property][:address][:city],
      state: data[:property][:address][:state],
      zip: data[:property][:address][:zip]
    }
  end

  def create_property(customer, data)
    customer.property = Property.new(property_info(data))
  end

  def create_policy(customer, data)
    policy_info = {
      carrier_name: data[:policy][:carrier_name],
      premium: data[:policy][:premium],
      coverages: data[:policy][:coverages]
    }
    customer.policy = Policy.new(policy_info)
  end

  def logger(customer)
    report = import.import_report
    report.fail_reasons.create(reason: customer.errors.full_messages.join(', '))
  end
end
