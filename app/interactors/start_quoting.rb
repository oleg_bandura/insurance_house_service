class StartQuoting
  include Interactor

  def call
    context.fail!(message: 'Quoting cannot be started without customers.') if customers.blank?
    customers.each do |customer|
      StartQuotingWorker.perform_async(customer.id, context.import_id)
    end
  end

  private

  def customers
    @customers ||= Customer.where(import_id: context.import_id)
  end
end
