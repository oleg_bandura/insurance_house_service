class ParseCsv
  include Interactor

  def call
    CSV.foreach(file, headers: true) do |row|
      ParseCsvWorker.perform_async(row.to_hash, import.id)
    end
    update_import_status
    add_new_import_report
  end

  private

  def import
    @import ||= Import.find(context.import_id)
  end

  def file
    file_upload = import.file
    file_upload.cache_stored_file!
    File.open(file_upload.path)
  end

  def add_new_import_report
    import.create_import_report
  end

  def update_import_status
    import.started!
  end
end
