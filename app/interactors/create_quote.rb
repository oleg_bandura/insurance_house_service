class CreateQuote
  include Interactor

  delegate :customer, :import_id, to: :context

  def call
    data = builder.build
    response = requester(data).request
    create_quote(response)
  end

  private

  def create_quote(response)
    Quote.create!(status: response['status'],
                  carrier_key: response['carrier_key'],
                  message: response['message'],
                  premium: response.dig('quote', 'premium'),
                  coverages: {
                    deductible: response.dig('quote', 'deductible'),
                    dwelling_coverage: response.dig('quote', 'dwelling_coverage')
                  },
                  import_id: import_id,
                  customer_id: customer.id)
  end

  def builder
    Quoting::Builder.new(customer)
  end

  def requester(data)
    Quoting::Requester.new(data)
  end
end
