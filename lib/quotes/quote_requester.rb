require 'rest-client'

module Quotes
  class QuoteRequester
    def initialize(data)
      @data = data
      @url = 'https://thawing-citadel-54366.herokuapp.com/api/quote'
    end

    def request
      response = RestClient.post(@url, @data)
      response.body
    rescue RestClient::Exception => error
      error.http_body
    end
  end
end
