module Mappers
  class Privatbank
    class << self
      def format(row)
        { customer: customer_attributes(row),
          property: property_attributes(row),
          policy: policy_attributes(row) }
      end

      private

      def customer_attributes(row)
        {
          first_name: row['first_name'],
          last_name: row['last_name'],
          fico_score: row['fico_score'],
          marital_status: row['marital_status'],
          date_of_birth: row['date_of_birth'],
          phone: row['phone'],
          email: row['email']
        }
      end

      def property_attributes(row)
        {
          property_type: row['property_type'],
          residency_type: row['residency_type'],
          address: {
            address1: row['address_line1'],
            city: row['city'],
            state: row['state'],
            zip: row['zip']
          }
        }
      end

      def policy_attributes(row)
        {
          carrier_name: row['carrier_name'],
          premium: row['premium'],
          coverages: parse_coverages(row)
        }
      end

      def parse_coverages(row)
        JSON.parse(row['coverages'].gsub('=>', ':'))
      end
    end
  end
end
