module Mappers
  class MapperFinder
    def self.find_by_partner_key(partner_key)
      partner = Partner.find_by(key: partner_key)
      partner_name = partner.name.capitalize
      "Mappers::#{partner_name}".constantize
    end
  end
end
