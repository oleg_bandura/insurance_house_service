unless Rails.env.production?
  require 'factory_bot'
  require 'ffaker'

  namespace :testdata do
    include FactoryBot::Syntax::Methods

    desc 'Fill database with sample data'
    task seeds: :environment do
      import = create(:import)
      500.times do
        create(:quote, import: import)
      end
    end
  end
end
