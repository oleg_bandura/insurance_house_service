module Quoting
  class Requester
    def initialize(data)
      @data = data
      @url = 'https://thawing-citadel-54366.herokuapp.com/api/quote'
    end

    def request
      response = RestClient.post(@url, @data)
      JSON.parse(response.body)
    rescue RestClient::NotFound => error
      error.response
    rescue RestClient::Exception => error
      error.response
    end
  end
end
