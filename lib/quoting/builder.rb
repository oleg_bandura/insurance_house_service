module Quoting
  class Builder
    def initialize(customer)
      @customer = customer
      @property = @customer.property
      @policy   = @customer.policy
    end

    def build
      { customer: customer_attributes,
        property: property_attributes,
        policy:   policy_attributes }
    end

    private

    attr_reader :customer, :property, :policy

    def format_date(date)
      date.strftime('%m/%d/%Y')
    end

    def customer_attributes
      {
        first_name: customer.first_name,
        last_name: customer.last_name,
        fico_score: customer.fico_score,
        date_of_birth: format_date(customer.date_of_birth),
        marital_status: customer.marital_status,
        phone: customer.phone,
        email: customer.email
      }
    end

    def property_attributes
      {
        address: {
          address1: property.address_line1,
          city: property.city,
          state: property.state,
          zip: property.zip
        },
        property_type: property.property_type,
        residency_type: property.residency_type
      }
    end

    def policy_attributes
      {
        carrier_name: policy.carrier_name,
        premium: policy.premium,
        coverages: policy.coverages
      }
    end
  end
end
