# frozen_string_literal: true

Rails.application.routes.draw do
  get 'pages/error'
  get 'imports/generate_report'
  get 'imports/parse_csv_file'
  get 'imports/start_quoting'
  root 'pages#main'

  devise_for :users

  resources :imports, only: %i[new create show index] do
    resources :quotes, only: %i[index show]
  end
  resources :users
end
