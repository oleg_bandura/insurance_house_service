# string_literal: true

# s hook to configure devise mailer, warden hooks and so forth.
#  these configuration options can be set straight in your model.
Devise.setup do |config|
  # ret key used by Devise. Devise uses this key to generate
  # tokens. Changing this key will render invalid all existing
  # ation, reset password and unlock tokens in the database.
  # will use the `secret_key_base` as its `secret_key`
  # ult. You can change it below and use your own secret key.

  # troller configuration
  # re the parent class to the devise controllers.
  # parent_controller = 'DeviseController'

  # ler Configuration
  # re the e-mail address which will be shown in Devise::Mailer,
  # at it will be overwritten if you use your own mailer class
  # fault "from" parameter.
  config.mailer_sender = 'config-initializers-devise@example.com'

  # re the class responsible to send e-mails.
  # mailer = 'Devise::Mailer'

  # re the parent class responsible to send e-mails.
  # parent_mailer = 'ActionMailer::Base'

  #  configuration
  # d configure the ORM. Supports :active_record (default) and
  # d (bson_ext recommended) by default. Other ORMs may be
  # le as additional gems.
  require 'devise/orm/active_record'

  # figuration for any authentication mechanism
  # re which keys are used when authenticating a user. The default is
  # mail. You can configure it to use [:username, :subdomain], so for
  # icating a user, both parameters are required. Remember that those
  # ers are used only when authenticating and not when retrieving from
  # need permissions, you should implement that in a before filter.
  #  also supply a hash where the value is a boolean determining whether
  # authentication should be aborted when the value is not present.
  # authentication_keys = [:email]

  # request_keys = []

  # re which authentication keys should be case-insensitive.
  # eys will be dowcased upon creating or modifying a user and when used
  # enticate or find a user. Default is :email.
  config.case_insensitive_keys = [:email]

  # re which authentication keys should have whitespace stripped.
  # eys will have whitespace before and after removed upon creating or
  # ng a user and when used to authenticate a user. Default is :email.
  config.strip_whitespace_keys = [:email]

  #  authentication through request.params is enabled. True by default.
  # be set to an array that will enable params authentication only for the
  # trategies, for example, `config.params_authenticatable = [:database]` will
  # it only for database (email + password) authentication.
  # params_authenticatable = true

  #  authentication through HTTP Auth is enabled. False by default.
  # be set to an array that will enable http authentication only for the
  # trategies, for example, `config.http_authenticatable = [:database]` will
  # it only for database authentication. The supported strategies are:
  # se      = Support basic authentication with authentication key + password
  # http_authenticatable = false

  # status code should be returned for AJAX requests. True by default.
  # http_authenticatable_on_xhr = true

  # lm used in Http Basic Authentication. 'Application' by default.
  # http_authentication_realm = 'Application'

  #  change confirmation, password recovery and other workflows
  # ve the same regardless if the e-mail provided was right or wrong.
  # t affect registerable.
  # paranoid = true

  # ult Devise will store the user in session. You can skip storage for
  # lar strategies by setting this option.
  # that if you are skipping storage for all authentication paths, you
  # t to disable generating routes to Devise's sessions controller by
  #  skip: :sessions to `devise_for` in your config/routes.rb
  config.skip_session_storage = [:http_auth]

  # ult, Devise cleans up the CSRF token on authentication to
  # SRF token fixation attacks. This means that, when using AJAX
  # s for sign in and sign up, you need to get a new CSRF token
  # e server. You can disable this option at your own risk.
  # clean_up_csrf_token_on_authentication = true

  # lse, Devise will not attempt to reload routes on eager load.
  # n reduce the time taken to boot the app but if your application
  # s the Devise mappings to be loaded during boot time the application
  # oot properly.
  # reload_routes = true

  # figuration for :database_authenticatable
  # ypt, this is the cost for hashing the password and defaults to 11. If
  # ther algorithms, it sets how many times you want the password to be hashed.
  #
  # g the stretches to just one in testing will increase the performance of
  # st suite dramatically. However, it is STRONGLY RECOMMENDED to not use
  #  less than 10 in other environments. Note that, for bcrypt (the default
  # hm), the cost increases exponentially with the number of stretches (e.g.
  #  of 20 is already extremely slow: approx. 60 seconds for 1 calculation).
  config.stretches = Rails.env.test? ? 1 : 11

  # a pepper to generate the hashed password.

  # notification to the original email when the user's email is changed.
  # send_email_changed_notification = false

  # notification email when the user's password is changed.
  # send_password_change_notification = false

  # figuration for :confirmable
  # d that the user is allowed to access the website even without
  # ing their account. For instance, if set to 2.days, the user will be
  #  access the website for two days without confirming their account,
  # will be blocked just in the third day. Default is 0.days, meaning
  # r cannot access the website without confirming their account.
  # allow_unconfirmed_access_for = 2.days

  # d that the user is allowed to confirm their account before their
  # ecomes invalid. For example, if set to 3.days, the user can confirm
  # ccount within 3 days after the mail was sent, but on the fourth day
  # ccount can't be confirmed with the token any more.
  #  is nil, meaning there is no restriction on how long a user can take
  # confirming their account.
  # confirm_within = 3.days

  # , requires any email changes to be confirmed (exactly the same way as
  #  account confirmation) to be applied. Requires additional unconfirmed_email
  # d (see migrations). Until confirmed, new email is stored in
  # rmed_email column, and copied to email column on successful confirmation.
  config.reconfirmable = true

  #  which key will be used when confirming an account
  # confirmation_keys = [:email]

  # figuration for :rememberable
  # e the user will be remembered without asking for credentials again.
  # remember_for = 2.weeks

  # ates all the remember me tokens when the user signs out.
  config.expire_all_remember_me_on_sign_out = true

  # , extends the user's remember period when remembered via cookie.
  # extend_remember_period = false

  #  to be passed to the created cookie. For instance, you can set
  #  true in order to force SSL only cookies.
  # rememberable_options = {}

  # figuration for :validatable
  # or password length.
  config.password_length = 6..128

  # egex used to validate email formats. It simply asserts that
  # d only one) @ exists in the given string. This is mainly
  #  user feedback and not to assert the e-mail validity.
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/

  # figuration for :timeoutable
  # e you want to timeout the user session without activity. After this
  # e user will be asked for credentials again. Default is 30 minutes.
  # timeout_in = 30.minutes

  # figuration for :lockable
  #  which strategy will be used to lock an account.
  # _attempts = Locks an account after a number of failed attempts to sign in.
  #           = No lock strategy. You should handle locking by yourself.
  # lock_strategy = :failed_attempts

  #  which key will be used when locking and unlocking an account
  # unlock_keys = [:email]

  #  which strategy will be used to unlock an account.
  # = Sends an unlock link to the user email
  # = Re-enables login after a certain amount of time (see :unlock_in below)
  # = Enables both strategies
  # = No unlock strategy. You should handle unlocking by yourself.
  # unlock_strategy = :both

  # of authentication tries before locking an account if lock_strategy
  # ed attempts.
  # maximum_attempts = 20

  # terval to unlock the account if :time is enabled as unlock_strategy.
  # unlock_in = 1.hour

  #  the last attempt before the account is locked.
  # last_attempt_warning = true

  # figuration for :recoverable
  #
  #  which key will be used when recovering the password for an account
  # reset_password_keys = [:email]

  # terval you can reset your password with a reset password key.
  # ut a too small interval or your users won't have the time to
  # their passwords.
  config.reset_password_within = 6.hours

  # t to false, does not sign a user in automatically after their password is
  # Defaults to true, so a user is signed in automatically after a reset.
  # sign_in_after_reset_password = true

  # figuration for :encryptable
  # ou to use another hashing or encryption algorithm besides bcrypt (default).
  #  use :sha1, :sha512 or algorithms from others authentication tools as
  # nce_sha1, :authlogic_sha512 (then you should set stretches above to 20
  # ault behavior) and :restful_authentication_sha1 (then you should set
  # es to 10, and copy REST_AUTH_SITE_KEY to pepper).
  #
  #  the `devise-encryptable` gem when using anything other than bcrypt
  # encryptor = :sha512

  # pes configuration
  # oped views on. Before rendering "sessions/new", it will first check for
  # sessions/new". It's turned off by default because it's slower if you
  # ng only default views.
  # scoped_views = false

  # re the default scope given to Warden. By default it's the first
  # role declared in your routes (usually :user).
  # default_scope = :user

  # s configuration to false if you want /users/sign_out to sign out
  # e current scope. By default, Devise signs out all scopes.
  # sign_out_all_scopes = true

  # igation configuration
  # he formats that should be treated as navigational. Formats like
  # should redirect to the sign in page when the user does not have
  #  but formats like :xml or :json, should return 401.
  #
  # have any extra navigational formats, like :iphone or :mobile, you
  # add them to the navigational formats lists.
  #
  # *" below is required to match Internet Explorer requests.
  # navigational_formats = ['*/*', :html]

  # ault HTTP method used to sign out a resource. Default is :delete.
  config.sign_out_via = :delete

  # iAuth
  # ew OmniAuth provider. Check the wiki for more information on setting
  # our models and hooks.
  # omniauth :github, 'APP_ID', 'APP_SECRET', scope: 'user,public_repo'

  # den configuration
  # want to use other strategies, that are not supported by Devise, or
  # the failure app, you can configure them inside the config.warden block.
  #
  # warden do |manager|
  # er.intercept_401 = false
  # er.default_strategies(scope: :user).unshift :some_external_strategy

  # ntable engine configurations
  # ing Devise inside an engine, let's call it `MyEngine`, and this engine
  # table, there are some extra configurations to be taken into account.
  # lowing options are available, assuming the engine is mounted as:
  #
  # nt MyEngine, at: '/my_engine'
  #
  # ter that invoked `devise_for`, in the example above, would be:
  # router_name = :my_engine
  #
  # ing OmniAuth, Devise cannot automatically set OmniAuth path,
  # need to do it manually. For the users scope, it would be:
  # omniauth_path_prefix = '/my_engine/users/auth'
end
